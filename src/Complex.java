public class Complex {

    private double dRl, dImgnr;

    public Complex (double dRl, double dImgnr) {
        this.dRl = dRl;
        this.dImgnr = dImgnr;
    }

    public  double getdRl() {
        return dRl;
    }

    public double getdImgnr() {
        return dImgnr;
    }

    private double getModule() {
        return Math.sqrt(this.dRl*this.dRl + this.dImgnr*this.dImgnr);
    }

    public static Complex sum(Complex complex1, Complex complex2) {
        return new Complex(complex1.getdRl() + complex2.getdRl(), complex1.getdImgnr() + complex2.getdImgnr());
    }

    public static Complex multiply(Complex complex1, Complex complex2) {
        return new Complex(complex1.getdRl()*complex2.getdRl() - complex1.getdImgnr()*complex2.getdImgnr(), complex1.getdRl()*complex2.getdImgnr() + complex1.getdImgnr()*complex2.getdRl());
    }

    public static Complex subtract(Complex complex1, Complex complex2) {
        return new Complex(complex1.getdRl() - complex2.getdRl(), complex1.getdImgnr() - complex2.getdImgnr());
    }

    public static Complex divide (Complex complex1, Complex complex2) {
        Complex temp = new Complex(complex2.getdRl(), (-1) * complex2.getdImgnr());
        temp = Complex.multiply(complex1, temp);
        double denominator = complex2.getdRl() * complex2.getdRl() + complex2.getdImgnr() * complex2.getdImgnr();
        return new Complex(temp.getdRl() / denominator, temp.getdImgnr() / denominator);
    }

    private double getArg() {
        if (this.dRl>0) {
            return Math.atan(dImgnr/dRl);
        } else {
            if (dRl<0 && dImgnr>0) {
                return Math.PI + Math.atan(dImgnr/dRl);
            } else {
                return -Math.PI + Math.atan(dImgnr/dRl);
            }
        }
    }

    public static Complex pow(Complex complex, int power) {
        double factor = Math.pow(complex.getModule(), power);
        return new Complex(factor * Math.cos(power * complex.getArg()), factor * Math.sin(power * complex.getArg()));
    }

    public static Complex[] sqrt(Complex complex) {
        double a = complex.getModule() / 2;
        Complex pos = new Complex(Math.sqrt(a + complex.getdRl() / 2), Math.signum(complex.getdImgnr()) * Math.sqrt(a - complex.getdRl() / 2));
        Complex neg = new Complex((-1) * pos.getdRl(), (-1) * pos.getdImgnr());
        Complex[] answer = {pos, neg};
        return answer;
    }

    private String sign() {
        if (dImgnr > 0) return " + ";
        else return " - ";
    }

    @Override
    public String toString() {
        String string;
        if (dImgnr == 1 || dImgnr == -1) {
            if (dRl == 0) {
                string = sign() + "i";
            } else {
                string = Double.toString(dRl) + sign() + "i";
            }
        } else {
            string = Double.toString(dRl) + sign() + Double.toString(Math.abs(dImgnr)) + "i";
        }
        return string;
    }

    @Override
    public boolean equals(Object obj) {
        if (this.getClass() != obj.getClass() || obj == null)
            return false;
        return true;
    }

    public static void main(String[] args) {
        Complex x = new Complex(2, 3);
        Complex y = new Complex(-1, 2);
        System.out.println("z1 = " + x + ",     z2 = " + y);

        Complex z;
        z = Complex.sum(x, y);
        System.out.println("+ : " + z);

        z = Complex.subtract(x, y);
        System.out.println("- : " + z);

        z = Complex.divide(x, y);
        System.out.println("/ : " + z);

        z = Complex.multiply(x, y);
        System.out.println(" * :" + z);

        z = Complex.pow(y, 2);
        System.out.println("Pow 2 of z2 : " + z);

        Complex b = new Complex(3, 4);
        Complex[] ans = Complex.sqrt(b);
        System.out.println("Sqrt of " + b + " = " + ans[0] + ",  " + ans[1]);

    }
}
